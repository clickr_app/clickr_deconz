# ClickrDeconz

## Configuration

https://dresden-elektronik.github.io/deconz-rest-doc/endpoints/sensors/button_events/#ikea-tradfri-round-5-button-remote

Environment variables:

| name | default | description |
|------|---------|-------------|
| `CLICKR_LOG_LEVEL` | `info` | Set the log level |

## API Key
1. Log in to phoscon web app: http://<host>/pwa
2. Navigate to Settings > Gateway > Advanced
3. Click "Authenticate App"
4. `curl -X POST "http://$DECONZ/api" --header 'Content-Type: application/json' --data-raw '{"devicetype": "clickr"}'`

## API
- Get all: `GET /api/<key> -> { config: { uuid: ... }, sensors: { ... }`
- Get gateway ID: `GET /api/<key>/config -> { uuid: "ae..." }`
- Get all buttons: `GET /api/<key>/sensors -> { [_key]: { name: "1/2A", uniqueid: "ec:...", config: { battery: 47 } } }` (ignore type: Daylight)

## Deconz App
- VNC <host>:5900

## Trigger configuration backup
- `curl -X POST "http://$DECONZ/api/57B9543A74/config/export"`
- Back up volume: `/opt/deCONZ/`
