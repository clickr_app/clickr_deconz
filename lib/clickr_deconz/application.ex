defmodule ClickrDeconz.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children =
      [
        # Starts a worker by calling: ClickrDeconz.Worker.start_link(arg)
        # {ClickrDeconz.Worker, arg}
        if(Application.fetch_env!(:clickr_deconz, :enable_remote_adapter),
          do: ClickrDeconz.RemoteAdapter
        ),
        if(Application.fetch_env!(:clickr_deconz, :enable_local_websocket_adapter),
          do: ClickrDeconz.LocalWebsocketAdapter
        )
      ]
      |> Enum.filter(fn x -> x end)

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ClickrDeconz.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
