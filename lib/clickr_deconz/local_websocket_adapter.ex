defmodule ClickrDeconz.LocalWebsocketAdapter do
  use ClickrDeconz.JsonAdapter
  require Logger

  alias ClickrDeconz.{JsonAdapter, RemoteAdapter}

  # Client API
  def start_link(_opts) do
    url = Application.fetch_env!(:clickr_deconz, :local_ws_url)

    WebSockex.start_link(url, __MODULE__, :initial,
      name: __MODULE__,
      handle_initial_conn_failure: true,
      async: true
    )
  end

  def send(msg) do
    WebSockex.cast(__MODULE__, {:send, msg})
  end

  # Callbacks (server API)
  @impl WebSockex
  def handle_connect(_conn, :initial) do
    Logger.info("connected")
    {:ok, :initial}
  end

  @impl WebSockex
  def handle_cast({:send, msg}, :initial) do
    Logger.debug("Sending frame with payload: #{inspect(msg)}")
    reply(msg, :initial)
  end

  @impl JsonAdapter
  def handle_json(%{} = msg, :initial) do
    Logger.debug("received event")
    RemoteAdapter.send("event", msg)
    {:ok, :initial}
  end

  @impl WebSockex
  def handle_disconnect(%{reason: reason}, _status) do
    Logger.error("disconnected -> reconnect (reason: #{inspect(reason)})")
    Process.sleep(1000)
    {:reconnect, :initial}
  end
end
