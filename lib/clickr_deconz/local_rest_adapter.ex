defmodule ClickrDeconz.LocalRestAdapter do
  require Logger

  def get_config() do
    case HTTPoison.get("#{base_url()}/config") do
      {:ok, %{status_code: 200, body: body}} -> Jason.decode!(body)
    end
  end

  def get_sensors() do
    case HTTPoison.get("#{base_url()}/sensors") do
      {:ok, %{status_code: 200, body: body}} -> Jason.decode!(body)
    end
  end

  defp base_url() do
    url = Application.fetch_env!(:clickr_deconz, :local_rest_url)
    key = Application.fetch_env!(:clickr_deconz, :local_api_token)
    "#{url}/#{key}"
  end
end
