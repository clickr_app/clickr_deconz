defmodule ClickrDeconz.RemoteAdapter do
  use ClickrDeconz.JsonAdapter
  require Logger

  alias ClickrDeconz.{JsonAdapter, LocalRestAdapter}

  @heartbeat_ms 30_000

  # Client API
  def start_link(_opts) do
    api_token = Application.fetch_env!(:clickr_deconz, :remote_api_token)
    url = Application.fetch_env!(:clickr_deconz, :remote_url)
    url = "#{url}?api_token=#{api_token}"

    WebSockex.start_link(url, __MODULE__, :initial,
      name: __MODULE__,
      handle_initial_conn_failure: true,
      async: true
    )
  end

  def send(type, msg) do
    WebSockex.cast(__MODULE__, {:send, type, msg})
  end

  # Callbacks (server API)
  @impl WebSockex
  def handle_connect(_conn, :initial) do
    Logger.info("connected")

    # work around: can't reply in handle_connect, and can't call WebSockex.send_frame (CallingSelfError)
    WebSockex.cast(self(), :join_channel_on_connect)
    Process.send_after(self(), :send_heartbeat, @heartbeat_ms)
    {:ok, {:wait_to_join, 1}}
  end

  @impl true
  def handle_info(:send_heartbeat, {status, ref}) do
    Logger.debug("heartbeat")
    Process.send_after(self(), :send_heartbeat, @heartbeat_ms)

    reply(
      %{event: "heartbeat", topic: "phoenix", payload: %{}, ref: ref},
      {status, ref + 1}
    )
  end

  @impl WebSockex
  def handle_cast(:join_channel_on_connect, {:wait_to_join, ref}) do
    Logger.debug("join")
    # %{"uuid" => id} = LocalRestAdapter.get_config()
    # %{gateway_id: id}
    payload = %{}

    reply(
      %{event: "phx_join", topic: "deconz", payload: payload, ref: ref},
      {:joining, ref + 1}
    )
  end

  def handle_cast({:send, type, msg}, {:joined, ref}) do
    Logger.debug("send #{inspect(type)}")

    reply(
      %{
        topic: "deconz",
        event: type,
        payload: msg,
        ref: ref
      },
      {:joined, ref + 1}
    )
  end

  @impl JsonAdapter
  def handle_json(
        %{"event" => "phx_reply", "payload" => %{"status" => "ok"}, "ref" => reply_ref},
        {:joining, ref}
      )
      when reply_ref == ref - 1 do
    Logger.debug("joined")
    {:ok, {:joined, ref}}
  end

  def handle_json(
        %{"event" => "phx_error"} = msg,
        state
      ) do
    Logger.error("remote error (reconnecting): #{inspect(msg)}")
    {:close, state}
  end

  def handle_json(%{"event" => "get_sensors"}, {:joined, ref}) do
    Logger.debug("get sensors")
    sensors = LocalRestAdapter.get_sensors()

    {:reply,
     %{
       topic: "deconz",
       event: "sensors",
       payload: sensors,
       ref: ref
     }, {:joined, ref + 1}}
  end

  def handle_json(%{"event" => "phx_reply", "topic" => _}, state) do
    {:ok, state}
  end

  @impl WebSockex
  def handle_disconnect(%{reason: %{code: 403}}, :initial) do
    Logger.error("disconnected due to 403 forbidden -> stop")
    {:ok, :auth_invalid}
  end

  @impl WebSockex
  def handle_disconnect(%{reason: reason}, _state) do
    Logger.error("disconnected -> reconnect (reason: #{inspect(reason)}")
    Process.sleep(1000)
    {:reconnect, :initial}
  end
end
