import Config

config :clickr_deconz,
  local_ws_url: System.get_env("DECONZ_WS_URL", "ws://deconz:443"),
  local_rest_url: System.get_env("DECONZ_REST_URL", "http://deconz/api"),
  local_api_token: System.get_env("DECONZ_API_TOKEN"),
  remote_url: System.get_env("CLICKR_WS_URL", "wss://klassenknopf.de/api/websocket"),
  remote_api_token: System.get_env("CLICKR_API_TOKEN")

config :logger, level: String.to_atom(System.get_env("CLICKR_LOG_LEVEL", "debug"))
