import Config

# Print only warnings and errors during test
config :logger, level: :warn

config :clickr_deconz,
  enable_local_websocket_adapter: false,
  enable_remote_adapter: false
