ARG ELIXIR_VERSION=1.14.0
ARG OTP_VERSION=25.1.1
ARG ALPINE_VERSION=3.16.2

ARG BUILDER_IMAGE="hexpm/elixir:${ELIXIR_VERSION}-erlang-${OTP_VERSION}-alpine-${ALPINE_VERSION}"
ARG RUNNER_IMAGE="alpine:${ALPINE_VERSION}"

# Builder
FROM $BUILDER_IMAGE AS builder

ENV MIX_ENV=prod
RUN mix local.hex --force
RUN mix local.rebar --force

COPY mix.exs mix.lock ./
RUN mix deps.get --only prod
RUN mix deps.compile

COPY config config
COPY lib lib

RUN mix compile
RUN mix release 

# Runner
FROM $RUNNER_IMAGE

RUN apk add --no-cache openssl ncurses-libs libstdc++

WORKDIR /app
ENV HOME=/app

COPY --from=builder _build/prod/rel/clickr_deconz/ ./
RUN set -eux
RUN ln -nfs bin/clickr_deconz ./entry

CMD [ "./entry", "start" ]
